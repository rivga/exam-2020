<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Interview extends Model
{
    protected $fillable = ['date','brief', 'candidate_id','user_id'];
    
    public function candidates()
    {
        return $this->belongsTo('App\Candidate','candidate_id');
    }      

    public function users()
    {
        return $this->belongsTo('App\User','user_id');
    }      
}



