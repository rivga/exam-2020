<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' => '2020.07.14',
                'brief' => 'the candidate showed interest and is suitable for the job',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'date' => '2020.07.14 ',
                'brief' => 'the candidate isnt suitable',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                      
            ]);            
    }
}
