@extends('layouts.app')

@section('title', 'Interviews')

@section('content')

<h1>Interviews details</h1>
<table class = "table table-dark">
<div><a href =  "{{url('/interviews/create')}}"> Add a new interview</a></div>
    <!-- the table data -->
   
         <tr>
            <th>Id</th><th>Date</th><th>Brief</th><th>Candidate_id</th><th>User_id</th><th>Created</th><th>Updated</th>
        </tr>
        
        @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->date}}</td>
            <td>{{$interview->brief}}</td>
            <td>{{$interview->Candidate_id}}</td>
            <td>{{$interview->User_id}}</td>
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td> 
            <td>
            <td>
                                
            </td>
        </tr>   
        @endforeach 
        
@endsection