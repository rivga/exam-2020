@extends('layouts.app')

@section('title', 'Create Interview')

@section('content')
        <h1>Create Interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "date">Interview date</label>
            <input type = "date" class="form-control" name = "date">
        </div>     
        <div class="form-group">
            <label for = "brief">Interview brief</label>
            <input type = "text" class="form-control" name = "brief">
        </div> 
        
        <div class="form-group row">
                            <label for="candidate_id" class="col-md-8 col-form-label text-md-left">candidate_id</label>
                            <div class="col-md-6">
                                <select class="form-control" name="candidate_id">                                                                         
                                   @foreach ($candidates as $candidate)
                                     <option value="{{ $candidate->id}}"> 
                                         {{ $candidate->name }} 
                                     </option>
                                   @endforeach    
                                 </select>
                            </div>
        </div>

        <div class="form-group row">
                            <label for="user_id" class="col-md-8 col-form-label text-md-left">user_id</label>
                            <div class="col-md-6">
                                <select class="form-control" name="user_id">                                                                         
                                   @foreach ($users as $user)
                                     <option value="{{ $user->id}}"> 
                                         {{ $user->name }} 
                                     </option>
                                   @endforeach    
                                 </select>
                            </div>
        </div>

        <div>
            <input type = "submit" name = "submit" value = "Create Interview">
        </div>                       
        </form>    
@endsection
